Genomic DNA (gDNA) Extraction Protocol

Materials:

| - Ice Bucket w/ Ice | - 1000uL pipette and tips |
| --- | --- |
| - 200uL pipette and tips | - One (1) 2mL microcentrifuge tube |
| - Four (4) 1.7mL microcentrifuge tubes | - Microcentrifuge Tube Rack |
| - 250uL Solution A (see Appendix A below) | - 35uL Potassium Acetate (KAc) |
| - 285uL Phenol-Chloroform | - 150uL 100% Isopropanol |
| - 1mL 70% Ethanol (EtOH) | - 30uL Nuclease Free H2O |
| - homogenizer |
 |

Equipment:

| - Heating Block | - Centrifuge |
| --- | --- |
| - Fume Hood |
 |

1. Place one (1) **1.7mL microcentrifuge tube** on ice.

2. Add flies (can be as few as one or as many as 25-30) to the **1.7mL microcentrifuge tube**. Return tube to ice.

3. Add 250uL of **Solution A** to the tube and homogenize with homogenizer by continuously pressing flies against the side of the tube until all that is visible are chunks of wings.###1


**NOTE** : It will NOT work to homogenize flies by pressing against bottom of the tube, as there is considerable space between where the homogenizer can reach and the bottom of the tube. Be sure to use the side of the tube. **NOTE** : If performing protocol on multiple samples, return tube to ice while performing this step on the other samples so that DNA does not degrade. ### 23


4. Incubate for 30 minutes at 70°C.

5. Add **35uL KAc** and mix by forcefully flicking the tube ~10 times.

6. Incubate on ice for 30 minutes.

7. Centrifuge at 13,000 RPM for 5 minutes.

8. The resultant solution will have three layers: one translucent top layer that contains the DNA, a white interphase containing proteins, and a bottom opaque layer containing lipids. Pipette off the top layer being sure to leave the interphase and bottom layer and transfer to a new **1.7mL microcentrifuge tube.**

9. Add one volume **(285uL) of Phenol-Chloroform** from the Kensington fridge (4°C) to the new tube. Mix by inverting the tube a gently shaking it until liquid phases become mixed. Centrifuge for five (5) minutes at 13,000 RPM.

10. Repeat steps eight (8) and nine (9), then 8 one more time.

11. Add **150uL 100% Isopropanol** and shake by inverting tube several times. Centrifuge to a pellet for five (5) minutes at 10,000 RPM. ### 3

12. Pipette off and discard supernatant, being careful not to lose the pellet.

13. Wash the pellet with **1mL 70% Ethanol (EtOH)**. Centrifuge at 13,000 RPM for five (5) minutes.

14. Pipette out and discard as much Ethanol as possible while still maintaining the pellet.

15. Dry the pellet for 10 minutes by leaving tube propped open on its side inside the fume hood.

16. Resuspend the pellet by adding **30uL of NF H**** 2 ****O**

**Appendix A**

Solution A:

Tris HCl 0.1M (pH 9.0)

EDTA 0.1M

SDS 1%

For every 10mL of Solution A:

1mL 1M Tris HCl

2mL 0.5M EDTA

0.5mL 20% SDS

6.5mL NF H2O

[1](#sdfootnote1anc) It will NOT work to homogenize flies by pressing against bottom of the tube, as there is considerable space between where the homogenizer can reach and the bottom of the tube. Be sure to use the side of the tube.

[2](#sdfootnote2anc) If performing protocol on multiple samples, return tube to ice while performing this step on the other samples so that DNA does not degrade.

[3](#sdfootnote3anc) If procedure cannot be completed in one sitting, after step 11 the solution can be placed in –20°C until ready to complete.
